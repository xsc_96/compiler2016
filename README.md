A compiler for modified C language(named magic_C)

4 step:
1.词法分析,语法分析: 用ANTLR完成, 生成visitor模式框架
2.语义分析, 类型检查: 基于Visitor的访问模式进行check, 生成符号表
3.中间代码: 采用三地址码, ILOC改, Call简化
4.MIPS: Memory to memory model+优化+伪寄存器分配